from django.db import models
from django.conf import settings


class BinVO(models.Model):
    import_href = models.CharField(max_length=200, blank=True, null=True, unique=True)
    closet_name = models.CharField(max_length=100)
    bin_number = models.PositiveSmallIntegerField(blank=True, null=True)
    bin_size = models.PositiveSmallIntegerField(blank=True, null=True)


    def __str__(self):
        return f"{self.closet_name} {self.bin_number} {self.bin_size}"





class Shoes(models.Model):
    manufacturer = models.CharField(max_length=126)
    shoe_name = models.CharField(max_length=126)
    shoe_color = models.CharField(max_length=56)
    shoe_photo_url = models.URLField(null=True)
    bin = models.ForeignKey(
        BinVO,
        related_name="shoes",
        on_delete=models.CASCADE,
    )
