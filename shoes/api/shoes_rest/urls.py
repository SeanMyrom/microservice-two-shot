from django.urls import path
from .views import list_of_shoes, shoe_detail


urlpatterns = [
    path("shoes/", list_of_shoes, name="list_of_shoes"),
    path('shoes/<int:pk>/', shoe_detail, name="shoe_detail"),
    path(
        "bins/<int:bin_vo_id>/shoes/",
        list_of_shoes,
        name="list_of_shoes",
    ),
]
