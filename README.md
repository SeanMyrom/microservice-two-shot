# Wardrobify

Team:

* Sean - Hats
* Heath - Shoes

## Design
Project Wardobify will contain a hats microservice and a shoes microservice.  These will be children microservices under the wardrobe parent.

HOW TO RUN:
Fork and clone the project from the link https://gitlab.com/SeanMyrom/microservice-two-shot
change your working directory to the new cloned repository.
run the following commands:
 1. docker volume create pgdata
 2. docker compose build
 3. docker compose up

 When the docker containers are up and running navigate to http://localhost:3000/ to see the page.
 navigation will be at the top of the page.
 You can add or delete hats by clicking either "create a hat" or "delete".


 URLS/PORTS:
 http://localhost:3000/
 http://localhost:3000/hats   to see the list of hats
 http://localhost:3000/hats/new     to create a new hat


## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.

The hats microservice will contain a "hats" model that will contain data relating to the hats fabric, style_name, color, location, and a picture.  There will be a LOCATION value object that is using information from the location model located in wardrobe.models.
The front end will utilize React (pulling data from the RESTful API) and you will be able to create a new hat for the wardrobe including its location and a picture.  You will also have the option to delete a hat.
