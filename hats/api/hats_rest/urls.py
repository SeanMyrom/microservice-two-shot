from django.urls import path

from .views import list_of_hats, hats_detail


urlpatterns = [
    path("hats/", list_of_hats, name= "list_of_hats"),
    path('hats/<int:pk>/', hats_detail, name="hats_detail"),
    path(
        "locations/<int:location_vo_id>/hats/",
        list_of_hats,
        name="list_of_hats",
    ),
]
